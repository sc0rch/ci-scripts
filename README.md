# EWA
> English Words Ambassador
>
> CI scripts for GitLab repository.
>
> Tracker: https://lithiumlab.atlassian.net/browse/EW

## Stages

 - changelog - Verifies changelog entries (merge request, develop or release tag).
 - quality - Runs various linters & checks.
 - tests - Runs unit tests.
 - build - Builds package.
 - deploy - Deploys package to AppCenter group.
 - build_appstore - Resignes & exports package for appstore.
 - submit_appstore - Submits exported package to Test Flight / for appstore release.

## How To Build

### Merge Request

Steps:
 - Push your latest changes, if any.
 - Resolve WIP Status for merge request.
 - Add changelog entries with JIRA task number EW-9999.
 - Verify that new pipeline has started.

Example:
```
## [Unreleased]
### Added
- Add Games tab EW-3668
```

### Develop
 - Will start automatically in most scenarios (aka push or merge).
 - Uses available entries from [Unreleased] section of changelog, can be empty.
 - Run Manully: add SKIP_EWA=true or SKIP_KIDS=true as variables to skip builds for specified projects.

### Release

Steps:
 - Create branch from develop with name "release/<project>/<version-tag>", for example: "release/ewa/5.6.1".
 - Set app marketing version to 5.6.1 to synchronize it with future release tag.
 - Set build number to 5.6.1.1 as the first release candidate version.
 - Move [Unreleased] entries to [5.6.1] section of changelog.
 - Commit all changes.
 - Verify that new pipeline has started.

If any bugs found during QA:
 - Increase build number to 5.6.1.2 (next release candidate).
 - Commit changes and new pipeline will start.

To finalise release:
 - Merge release into develop and master.
 - Add project-tag, i.e.: ewa-5.6.1

## Tags

You can add any tag to the title of merge request or to the commit message:

 - `[skip ci]` or `[ci skip]`: Skip entire pipeline.

 - `[no-quality]`: Skip Code Climate & linter steps.

 - `[no-deploy]`: Skip deploy, merge requests only.

 - `[no-tests]`: Skip unit tests.

Project-specific tags:

 - `[no-kids]`: Skip EWA-kids part of pipeline.

 - `[no-ewa]`: Skip EWA-ios part of pipeline.
